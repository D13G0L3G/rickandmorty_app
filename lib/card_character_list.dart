import 'package:flutter/material.dart';
import 'card_character.dart';

class CardCharacterList extends StatelessWidget{
  @override
  Widget build(BuildContext context) {

    return Container(
      margin: EdgeInsets.only(top:130),
      child: Column(
        children: [
          CardCharacter(),
          CardCharacter(),
          CardCharacter(),
          CardCharacter(),
          CardCharacter(),
          CardCharacter(),
          CardCharacter(),
          CardCharacter(),
          CardCharacter(),
          CardCharacter(),
          CardCharacter(),
          CardCharacter(),
          CardCharacter(),
          CardCharacter(),
          CardCharacter(),
          CardCharacter(),
          CardCharacter(),
          CardCharacter(),
          CardCharacter(),
          CardCharacter(),
        ],
      ),
    );
  }

}