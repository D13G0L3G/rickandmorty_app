import 'package:flutter/material.dart';

class HeaderAppBar extends StatelessWidget{
  @override
  Widget build(BuildContext context) {

    String pathImage = "assets/img/ramtitle.png";

    final contentHeader = Container(
      width: 250.0,
      height: 100.0,
      child: Image(image: AssetImage(pathImage)),
      alignment: Alignment(0.0, -0.9),
    );

    return ClipPath(
      clipper: BackgroundClipper(),
      child: Container(
        height: 150,
        width: double.infinity,
        decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: [
                  Color(0XFF3E9424),
                  Color(0XFFA0CD3D),
                ],
                begin: FractionalOffset(0.2, 0.0),
                end: FractionalOffset(1.0, 0.6),
                stops: [0.0, 0.5],
                tileMode: TileMode.clamp
            )
        ),
        child: Center(
         child: contentHeader,
        )
      ),
    );
  }

}

class BackgroundClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.lineTo(0, size.height-30);

    path.quadraticBezierTo(size.width/6, size.height-35, 2*size.width/6, size.height-20);
    path.quadraticBezierTo(size.width/2, size.height, 4*size.width/6, size.height-20);
    path.quadraticBezierTo(5*size.width/6, size.height-35, size.width, size.height-30);
    path.lineTo(size.width, 0);

    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return true;
  }
}