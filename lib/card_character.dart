import 'package:flutter/material.dart';

class CardCharacter extends StatelessWidget {
  String pathImage = "assets/img/ricksanchez.jpeg";

  CardCharacter();
  @override
  Widget build(BuildContext context) {


    final photo = Container(
      margin: EdgeInsets.only(top:15, right: 5, left: 15, bottom: 5),
      width: 80.0,
      height: 80.0,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        image: DecorationImage(
          image: AssetImage(pathImage),
          fit: BoxFit.cover
        )
      ),
    );

    final descriptionCharacter = Container(
      margin: EdgeInsets.only(top: 25, left: 10, right: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
              "Name: Rick Sanchez",
              style: TextStyle(
                fontFamily: 'SyneMono',
                fontSize: 15.0,
              )
          ),
          Text(
              "Species: Human",
              style: TextStyle(
                fontFamily: 'SyneMono',
                fontSize: 15.0,
              )
          ),
          Text(
              "Origin : Earth (C-137)",
              style: TextStyle(
                fontFamily: 'SyneMono',
                fontSize: 15.0,
            )
          )
        ],
      ),
    );

    final cardContent = Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        photo,
        descriptionCharacter
        //Text("Morty")
      ],
    );

    final cardCharacter = Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
      ),
      margin: EdgeInsets.all(5),
      child: SizedBox(
        height: 100.0,
        width: 1000.0,
        child: cardContent
      )
    );

    final containerCard = Container(
      margin: EdgeInsets.only(left: 10, right: 10),
      child: cardCharacter,
    );

    return containerCard;
  }
}